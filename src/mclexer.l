%{
#include "mcscanner.h"
%}

NATURAL     [0-9]+
INTEGER     -?{NATURAL}
FLOAT       {INTEGER}"."{NATURAL}
USERNAME    [A-Za-z0-9_]{3,16}
IP          ([0-9]{1,3}"."){3}[0-9]{1,3}
TIMESTAMP   [0-2][0-9]":"[0-6][0-9]":"[0-6][0-9]
LOGIN       "joined the game"
LOGOUT      "left the game"
ACHIEVEMENT "has just earned the achievement [".*"]"
MESSAGE         .+

%s INSIDE_SBRACKETS INSIDE_ABRACKETS INSIDE_PBRACKETS AFTER_TIMESTAMP AFTER_INFO AFTER_USERNAME AFTER_ABRACKET_USERNAME

%%
[ \t]+                /* eat up whitespace */

"\n"                                        { BEGIN(INITIAL);                   return NEWLINE_TOKEN; }
"["                                         { BEGIN(INSIDE_SBRACKETS);          return OPEN_SBRACKET_TOKEN; }
"]"                                         {                                   return CLOSED_SBRACKET_TOKEN; }
<AFTER_USERNAME>">"                         { BEGIN(AFTER_ABRACKET_USERNAME);   return CLOSED_ABRACKET_TOKEN; }
"<"                                         { BEGIN(INSIDE_ABRACKETS);          return OPEN_ABRACKET_TOKEN; }	    
">"                                         {                                   return CLOSED_ABRACKET_TOKEN; }
"("                                         { BEGIN(INSIDE_PBRACKETS);          return OPEN_PBRACKET_TOKEN; }
")"                                         {                                   return CLOSED_PBRACKET_TOKEN; }
":"                                         {                                   return COLON_TOKEN; }
"/"                                         {                                   return FSLASH_TOKEN; }
<INSIDE_SBRACKETS>"Server thread/INFO"/"]"  { BEGIN(AFTER_INFO);                return INFO_TOKEN; }
<INSIDE_SBRACKETS>{TIMESTAMP}/"]"           {                                   return TIMESTAMP_TOKEN; }
<AFTER_USERNAME>{LOGIN}                     {                                   return LOGIN_TOKEN; }
<AFTER_USERNAME>{LOGOUT}                    {                                   return LOGOUT_TOKEN; }
<AFTER_USERNAME>{ACHIEVEMENT}               {                                   return ACHIEVEMENT_TOKEN; }
{IP}                                        {                                   return IP_TOKEN; }
<AFTER_INFO>{USERNAME}                      { BEGIN(AFTER_USERNAME);            return USERNAME_TOKEN; }
<INSIDE_ABRACKETS>{USERNAME}/">"            { BEGIN(AFTER_USERNAME);            return USERNAME_TOKEN; }
<AFTER_ABRACKET_USERNAME>{MESSAGE}          {                                   return MESSAGE_TOKEN; }

<AFTER_USERNAME>"was shot by ".+                            |
"was pricked to death"										|
"walked into a cactus whilst trying to escape ".+			|
"was stabbed to death"										|
"was roasted in dragon breath".*							|
"drowned".*													|
"suffocated in a wall".*									|
"was squished too much"										|
"was squashed by ".+										|
"experienced kinetic energy".*								|
"removed an elytra while flying whilst trying to escape ".+	|
"blew up"													|
"was blown up by ".+										|
"was killed by ".+											|
"hit the ground too hard".*									|
"fell from a high place"									|
"fell off ".+												|
"fell into a patch of ".+									|
"was doomed to fall ".*										|
"fell too far and was finished by ".+						|
"was shot off some vines by ".+								|
"was shot off a ladder by ".+								|
"was blown from a high place by ".+							|
"went up in flames"											|
"burned to death"											|
"was burnt to a crisp whilst fighting ".+					|
"walked into fire whilst fighting ".+						|
"went off with a bang".*									|
"tried to swim in lava".*									|
"was struck by lightning".*									|
"was slain by ".+											|
"got finished off by ".+									|
"was fireballed by ".+										|
"starved to death"											|
"was poked to death by a sweet berry bush".*				|
"was killed trying to hurt ".+								|
"was impaled by ".+											|
"was speared by ".+											|
"fell out of the world"										|
"fell from a high place and fell out of the world"			|
"didn't want to live in the same world as ".+				|
"withered away".*											|
"was pummeled by ".+										|
"died because of ".+										|
"Actually, message was too long to deliver fully. Sorry! Here’s stripped version: ".* { return DEATH_TOKEN; }

.                                                                                       /* eat up any unmatched character */ 

%%

int yywrap (void) {
        return 1;
}
