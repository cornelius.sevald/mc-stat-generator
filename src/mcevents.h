#ifndef MCEVENTS_H
#define MCEVENTS_H

#include <time.h>

typedef struct event_node {
        time_t time;
        char user[256];
        int action;
        struct event_node *next;
} event_node;

typedef struct event_sequence {
        event_node *first;
} event_seq;

void add_event_node (event_seq *seq, event_node *node);
void init_event_sequence (event_seq *seq);
void destroy_event_sequence (event_seq *seq);
void append_event_sequence (event_seq *s1, event_seq *s2);
int event_sequence_length (event_seq *seq);

#endif /* MCEVENTS_H */
