#ifndef UTIL_H
#define UTIL_H

void  sort_strings (char *string_array[], int string_count);
FILE *make_tmp_file (char *template);

#endif /* UTIL_H */
