/* Small helper functions for writing json files. */

#ifndef JSONUTILS_H
#define JSONUTILS_H

#include <stdio.h>

void write_indent (FILE *fp, int indent_level);
void write_json (FILE *fp, int indent_level, const char *fmt, ...);
void write_int_stats (FILE *fp, int indent_level, int usercount,
                const char *label, char *usernames[], int stats[]);

#endif /* JSONUTILS_H */
