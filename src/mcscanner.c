#define _XOPEN_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "mcscanner.h"

extern int yylex ();
extern void yylex_destroy ();
extern int yylineno;
extern char *yytext;
extern FILE *yyin;

const char *token_names[] = {
        NULL,
        "newline",
        "open sbracket",
        "closed sbracket",
        "open abracket",
        "closed abracket",
        "open pbracket",
        "closed pbracket",
        "colon",
        "fslash",
        "info",
        "timestamp",
        "ip",
        "natural",
        "integer",
        "float",
        "username",
        "login",
        "logout",
        "achievement",
        "message",
        "death"
};

static event_node *create_node (char *timestamp, char *user, int action);

event_seq *scan_file (FILE *fp) {
        int token;
        char timestamp[256];
        char user[256];
        int action;
        event_seq *seq = malloc(sizeof(event_seq));
        seq->first = NULL;

        yyin = fp;
        token = -1;
        while (token) {
                if (token != -1) {
                        if (token != NEWLINE_TOKEN) {
                        /* Scan until next line */
                                while (token && token != NEWLINE_TOKEN) {
                                        token = yylex();
                                }
                        }
                }

                if ((token = yylex()) != OPEN_SBRACKET_TOKEN) {
                        continue;
                }
                if ((token = yylex()) != TIMESTAMP_TOKEN) {
                        continue;
                }
                strncpy(timestamp, yytext, 256);
                if ((token = yylex()) != CLOSED_SBRACKET_TOKEN) {
                        continue;
                }
                if ((token = yylex()) != OPEN_SBRACKET_TOKEN) {
                        continue;
                }
                if ((token = yylex()) != INFO_TOKEN) {
                        continue;
                }
                if ((token = yylex()) != CLOSED_SBRACKET_TOKEN) {
                        continue;
                }
                if ((token = yylex()) != COLON_TOKEN) {
                        continue;
                }
                /* Split desicion in two */
                if ((token = yylex()) != USERNAME_TOKEN &&
                    token           != OPEN_ABRACKET_TOKEN) {
                        continue;
                }
                switch (token) {
                        case USERNAME_TOKEN:
                                strncpy(user, yytext, 256);
                                if ((token = yylex()) != LOGIN_TOKEN &&
                                    token            != LOGOUT_TOKEN &&
                                    token            != ACHIEVEMENT_TOKEN &&
                                    token            != DEATH_TOKEN) {
                                        continue;
                                }
                                action = token;
                                break;
                        case OPEN_ABRACKET_TOKEN:
                                if ((token = yylex()) != USERNAME_TOKEN) {
                                        continue;
                                }
                                strncpy(user, yytext, 256);
                                if ((token = yylex()) != CLOSED_ABRACKET_TOKEN) {
                                        continue;
                                }
                                if ((token = yylex()) != MESSAGE_TOKEN) {
                                        continue;
                                }
                                action = token; /* i.e. MESSAGE_TOKEN */
                                break;
                }
                if ((token = yylex()) != NEWLINE_TOKEN) {
                        continue;
                }
                event_node *node = create_node(timestamp, user, action);
                add_event_node(seq, node);
        }
        return seq;
}


void scanner_destroy () {
        yylex_destroy();
}

static event_node *create_node (char *timestamp, char *user, int action) {
        struct tm btime = {0};
        // Allocate memory for the node.
        event_node *node = malloc(sizeof(event_node));
        // Convert the time stamp string into broken-down time.
        strptime(timestamp, "%H:%M:%S", &btime);
        // Convert the broken-down time into calendar time.
        node->time = mktime(&btime);
        // Copy the username into the node.
        strncpy(node->user, user, 256);
        node->action = action;

        node->next = NULL;

        return node;
}
