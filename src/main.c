#define _XOPEN_SOURCE
#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <zlib.h>
#include <getopt.h>
#include "mcscanner.h"
#include "mcevents.h"
#include "jsonutils.h"
#include "util.h"

/* CHUNK is the size of the memory chunk used by the zlib routines. */
#define CHUNK 0x4000

/* These are parameters to inflateInit2. See
http://zlib.net/manual.html for the exact meanings. */
#define windowBits 15
#define ENABLE_ZLIB_GZIP 32

#define MAX_USERS 100
#define MAX_OUT_PATH_SIZE 512

static void print_usage ();
static void print_help ();
static void get_stats (
                int achievement_stats[], int message_stats[],
                int death_stats[], time_t time_stats[],
                const event_seq *timeline);
static void write_stats_as_timeline (FILE *fp, const event_seq *timeline);
static void write_stats_as_json (FILE *fp, const event_seq *timeline);
static void write_stats_as_graphs (const char *out_dir, const event_seq *timeline);
static void create_graph (const char *out_dir, const char *name, const char *data_path,
                const char *y_label, const char *step_type, int usercount, char* usernames[]);
static int get_usernames (char *usernames[], const event_seq *seq);
static void inflate_file (const char *file_path, FILE *out_file);
static time_t get_date_from_file_path (const char *file_path);

static int help_flag;
static char *gnuplot_cmd = "gnuplot";

const char *progname;

int main (int argc, char *argv[]) {
        progname = argv[0];
        if (getenv("GNUPLOT") != NULL) {
                gnuplot_cmd = getenv("GNUPLOT");
        }

        char json_out_path[MAX_OUT_PATH_SIZE]     = {0};
        char timeline_out_path[MAX_OUT_PATH_SIZE] = {0};
        char graph_out_dir[MAX_OUT_PATH_SIZE]     = {0};

        int c;
        /* Parse command-line options */
        while (true) {
                if (help_flag) {
                        print_help();
                        exit(EXIT_SUCCESS);
                }
                static struct option long_options[] = {
                        {"help",        no_argument,       &help_flag, 1},
                        /* Options for output formats */
                        {"json",        required_argument, 0, 'j'},
                        {"timeline",    required_argument, 0, 't'},
                        {"graphs",      required_argument, 0, 'g'},
                        {0, 0, 0, 0}
                };
                int option_index = 0;

                c = getopt_long (argc, argv, "hj:t:g:",
                                long_options, &option_index);

                /* Detect the end of the options. */
                if (c == -1) {
                        break;
                }

                switch (c) {
                        case 0:
                                break;
                        case 'h':
                                help_flag = 1;
                                break;
                        case 'j':
                                strncpy(json_out_path, optarg, MAX_OUT_PATH_SIZE);
                                break;
                        case 't':
                                strncpy(timeline_out_path, optarg, MAX_OUT_PATH_SIZE);
                                break;
                        case 'g':
                                strncpy(graph_out_dir, optarg, MAX_OUT_PATH_SIZE);
                        case '?':
                                /* getopt_long already printed an error message. */
                                break;
                        default:
                                abort ();
                }
        }

        event_seq *timeline = malloc(sizeof(event_seq));
        init_event_sequence(timeline);

        /* Check if there are no more arguments. */
        if (optind >= argc) {
                fprintf(stderr,
                                "No log files specified\nTry %s --help\n",
                                progname);
                exit(EXIT_FAILURE);
        }
        const int file_count = argc - optind;
        char *file_paths[file_count];
        memcpy(file_paths, argv + optind, sizeof(char*) * file_count);

        // Sort the file paths so that they are in chronoligocal order.
        // This assumes they are all from the same directory.
        sort_strings(file_paths, file_count);
        for (int i = 0; i < file_count; i++) {
                const char *path = file_paths[i];

                time_t file_date = get_date_from_file_path(path);
                struct tm bdate = *localtime(&file_date);
                // Decompress the gzipped log files.
                FILE *inflated_file = tmpfile();
                inflate_file(path, inflated_file);
                rewind(inflated_file);

                // Get the event sequence from the file.
                event_seq *file_seq = scan_file(inflated_file);
                // Loop through each event and add the date of the log file.
                for (
                                event_node *current = file_seq->first;
                                current != NULL;
                                current = current->next
                    ) {
                        // Combine the date of the file name with the
                        // time of the event.
                        struct tm btime = *localtime(&current->time); struct tm bcombined = bdate; bcombined.tm_sec    = btime.tm_sec; bcombined.tm_min    = btime.tm_min;
                        bcombined.tm_hour   = btime.tm_hour;

                        time_t combined_time = mktime(&bcombined);
                        current->time = combined_time;
                }
                // Join it unto to total timeline & free the sequence.
                append_event_sequence(timeline, file_seq);
                free(file_seq);
                fclose(inflated_file);
        }

        if (strlen(json_out_path)) {
                printf("Writing json-formatted stats to '%s'...\n", json_out_path);
                // Write the statistics to a JSON file.
                FILE *json_stat_fp = fopen(json_out_path, "w");
                if (json_stat_fp == NULL) {
                        fprintf(stderr,
                                        "Could not open %s for writing.\n",
                                        json_out_path);
                        exit(EXIT_FAILURE);
                }

                write_stats_as_json (json_stat_fp, timeline);

                if (fclose(json_stat_fp)) {
                        fprintf(stderr,
                                        "Could not close %s.\n",
                                        json_out_path);
                        exit(EXIT_FAILURE);
                }
        }
        if (strlen(timeline_out_path)) {
                printf("Writing timeline to '%s'...\n", json_out_path);
                // Write the statistics to a timeline.
                FILE *timeline_fp = fopen(timeline_out_path, "w");
                if (timeline_fp  == NULL) {
                        fprintf(stderr,
                                        "Could not open %s for writing.\n",
                                        timeline_out_path);
                        exit(EXIT_FAILURE);
                }

                write_stats_as_timeline (timeline_fp, timeline);

                if (fclose(timeline_fp)) {
                        fprintf(stderr,
                                        "Could not close %s.\n",
                                        timeline_out_path);
                        exit(EXIT_FAILURE);
                }
        }
        if (strlen(graph_out_dir)) {
                printf("Creating graphs in folder '%s'...\n", graph_out_dir);
                write_stats_as_graphs(graph_out_dir, timeline);
        }

        destroy_event_sequence(timeline);
        free(timeline);
        scanner_destroy();

        exit(EXIT_SUCCESS);
}

static void print_usage () {
        printf("Usage: %s [OPTION]... [FORMAT <output>]... [FILE]...\n", progname);
}

static void print_help () {
        print_usage ();
        printf("Generate statistics from Minecraft server log files.\n");
        printf("The log files need to be gzip compressed,\nand have the date they where created as the file name.\n");

        printf("\n");
        printf("OPTIONS:\n");
        printf("--h, --help                 Show this help message and exit\n");

        printf("\n");
        printf("FORMATS:\n");
        printf("-j, --json <output>         Write the statistics in JSON format to the <output> file.\n");
        printf("-t, --timeline <output>     Write the statistics as a timeline to the <output> file.\n");
        printf("-g, --graphs <output>       Write the statistics as several PNG graphs to the <output> directory.\n");
}

static void get_stats (
                int achievement_stats[], int message_stats[],
                int death_stats[], time_t time_stats[],
                const event_seq *timeline) {
        char *usernames[MAX_USERS];
        int usercount = get_usernames(usernames, timeline);

        // Keep track of the last time each user logged in.
        time_t last_login[MAX_USERS] = {0};
        // Also keep track of wether a use is 'currently' logged in.
        // This is because partly a user can disconnect without logging in,
        // if they try to connect to the server and fail.
        bool logged_in[MAX_USERS] = {0};
        for (
                        event_node *current = timeline->first;
                        current != NULL;
                        current = current->next
            ) {
                int i = 0;
                // Increment `i` to point to the stats of the appropriate user.
                while (i < usercount) {
                        if (!strcmp(current->user, usernames[i++])) {
                                break;
                        }
                }
                i--;
                switch (current->action) {
                        case ACHIEVEMENT_TOKEN:
                                achievement_stats[i]++;
                                break;
                        case MESSAGE_TOKEN:
                                message_stats[i]++;
                                break;
                        case DEATH_TOKEN:
                                death_stats[i]++;
                                break;
                        case LOGIN_TOKEN:
                                last_login[i] = current->time;
                                logged_in[i] = true;
                                break;
                        case LOGOUT_TOKEN:
                                if (logged_in[i]) {
                                        time_stats[i] += (time_t) difftime(current->time, last_login[i]);
                                        logged_in[i] = false;
                                }
                                break;
                        default:
                                break;
                }
        }
        for (int i = 0; i < usercount; i++) {
                free(usernames[i]);
        }
}

static void write_stats_as_timeline (FILE *fp, const event_seq *timeline) {
        // Loop through the entire timeline and write every event.
        {
                int i = 0;
                for (
                                event_node *current = timeline->first;
                                current != NULL;
                                current = current->next, i++
                    ) {
                        struct tm *btime = localtime(&current->time);
                        char timestr[256];
                        strftime(timestr, 256, "%c", btime);

                        fprintf(fp, "[%s]\t%-16s\t%s\n",
                                        timestr, current->user,
                                        token_names[current->action]);
                }
        }


}

static void write_stats_as_json (FILE *fp, const event_seq *timeline) {
        char *usernames[MAX_USERS];
        int usercount = get_usernames(usernames, timeline);

        int    achievement_stats[MAX_USERS] = {0};
        int    message_stats[MAX_USERS]     = {0};
        int    death_stats[MAX_USERS]       = {0};
        time_t time_stats[MAX_USERS]        = {0};

        int indt_lvl = 0;

        get_stats (achievement_stats, message_stats, death_stats, time_stats, timeline);

        write_json(fp, indt_lvl++, "{\n");
        /* Achievement stats */
        write_int_stats(fp, indt_lvl, usercount, "achievements", usernames, achievement_stats);
        fprintf(fp, ",\n");
        /* Message stats */
        write_int_stats(fp, indt_lvl, usercount, "messages", usernames, message_stats);
        fprintf(fp, ",\n");
        /* Death stats */
        write_int_stats(fp, indt_lvl, usercount, "deaths", usernames, death_stats);
        fprintf(fp, ",\n");
        /* online time stats */
        write_json(fp, indt_lvl,    "\"%s\":\n", "online time");
        write_json(fp, indt_lvl++,  "{\n");
        for (int i = 0; i < usercount; i++) {
                char  *user  = usernames[i];
                time_t stat  = time_stats[i];

                int hours   = stat / 60 / 60;
                stat -= hours * 60 * 60;
                int minutes = stat / 60;
                stat -= minutes * 60;
                int seconds = stat;

                write_json(fp, indt_lvl,   "\"%s\":\n", user);
                write_json(fp, indt_lvl++, "{\n");
                write_json(fp, indt_lvl,   "\"hours\":   %d,\n", hours);
                write_json(fp, indt_lvl,   "\"minutes\": %d,\n", minutes);
                write_json(fp, indt_lvl,   "\"seconds\": %d\n",  seconds);
                write_json(fp, --indt_lvl, "}");
                if (i != usercount - 1) {
                        fprintf(fp, ",\n");
                } else {
                        fprintf(fp, "\n");
                }
        }
        write_json(fp, --indt_lvl, "}\n");
        write_json(fp, --indt_lvl, "}\n");

        for (int i = 0; i < usercount; i++) {
                free(usernames[i]);
        }
}

static void write_stats_as_graphs (const char *out_dir, const event_seq *timeline) {
        char *usernames[MAX_USERS];
        int usercount = get_usernames(usernames, timeline);

        int     achievement_stats[MAX_USERS] = {0};
        int     message_stats[MAX_USERS]     = {0};
        int     death_stats[MAX_USERS]       = {0};
        time_t  time_stats[MAX_USERS]        = {0};
        char    achievement_data_path[17]    = "/tmp/mcsg.XXXXXX";
        char    message_data_path[17]        = "/tmp/mcsg.XXXXXX";
        char    death_data_path[17]          = "/tmp/mcsg.XXXXXX";
        char    time_data_path[17]           = "/tmp/mcsg.XXXXXX";

        FILE   *achievement_fp = make_tmp_file(achievement_data_path);
        FILE   *message_fp     = make_tmp_file(message_data_path);
        FILE   *death_fp       = make_tmp_file(death_data_path);
        FILE   *time_fp        = make_tmp_file(time_data_path);

        // Keep track of the last time each user logged in.
        time_t last_login[MAX_USERS] = {0};
        // Also keep track of wether a use is 'currently' logged in.
        // This is because partly a user can disconnect without logging in,
        // if they try to connect to the server and fail.
        bool logged_in[MAX_USERS] = {0};

        /* Store all of the stats in the temporary files */
        for (
                        event_node *current = timeline->first;
                        current != NULL;
                        current = current->next
            ) {
                int i = 0;
                // Increment `i` to point to the stats of the appropriate user.
                while (i < usercount) {
                        if (!strcmp(current->user, usernames[i++])) {
                                break;
                        }
                }
                i--;
                switch (current->action) {
                        case ACHIEVEMENT_TOKEN:
                                achievement_stats[i]++;
                                fprintf(achievement_fp, "%llu", (unsigned long long) current->time);
                                for (int j = 0; j < usercount; j++) {
                                        fprintf(achievement_fp, "\t%d", achievement_stats[j]);
                                }
                                fprintf(achievement_fp, "\n");
                                break;
                        case MESSAGE_TOKEN:
                                message_stats[i]++;
                                fprintf(message_fp, "%llu", (unsigned long long) current->time);
                                for (int j = 0; j < usercount; j++) {
                                        fprintf(message_fp, "\t%d", message_stats[j]);
                                }
                                fprintf(message_fp, "\n");
                                break;
                        case DEATH_TOKEN:
                                death_stats[i]++;
                                fprintf(death_fp, "%llu", (unsigned long long) current->time);
                                for (int j = 0; j < usercount; j++) {
                                        fprintf(death_fp, "\t%d", death_stats[j]);
                                }
                                fprintf(death_fp, "\n");
                                break;
                        case LOGIN_TOKEN:
                                if (!logged_in[i]) {
                                        fprintf(time_fp, "%llu", (unsigned long long) current->time);
                                        for (int j = 0; j < usercount; j++) {
                                                if (logged_in[j]) {
                                                        time_stats[j] += (time_t) difftime(current->time, last_login[j]);
                                                        last_login[j] = current->time;
                                                }
                                                fprintf(time_fp, "\t%f", (double) time_stats[j] / 60.0 / 60.0);
                                        }
                                        fprintf(time_fp, "\n");
                                }

                                last_login[i] = current->time;
                                logged_in[i] = true;
                                break;
                        case LOGOUT_TOKEN:
                                if (logged_in[i]) {
                                        fprintf(time_fp, "%llu", (unsigned long long) current->time);
                                        for (int j = 0; j < usercount; j++) {
                                                if (logged_in[j]) {
                                                        time_stats[j] += (time_t) difftime(current->time, last_login[j]);
                                                        last_login[j] = current->time;
                                                }
                                                fprintf(time_fp, "\t%f", (double) time_stats[j] / 60.0 / 60.0);
                                        }
                                        fprintf(time_fp, "\n");

                                        logged_in[i] = false;
                                }
                                break;
                        default:
                                break;
                }
        }
        
        fclose(achievement_fp);
        fclose(message_fp);
        fclose(death_fp);
        fclose(time_fp);


        /* Create the graphs. */
        create_graph (out_dir, "achievements", achievement_data_path,
                        "Achievements", "steps", usercount, usernames);
        create_graph (out_dir, "messages", message_data_path,
                        "Messages", "steps", usercount, usernames);
        create_graph (out_dir, "deaths", death_data_path,
                        "Deaths", "steps", usercount, usernames);
        create_graph (out_dir, "time", time_data_path,
                        "Online time [Hours]", "lines", usercount, usernames);

        for (int i = 0; i < usercount; i++) {
                free(usernames[i]);
        }

}

static void create_graph (const char *out_dir, const char *name, const char *data_path,
                const char *y_label, const char *step_type, int usercount, char* usernames[]) {
        /* Store the plotting commands in a temporary file */
        FILE *cmd_fp = tmpfile();

        char out_path[MAX_OUT_PATH_SIZE] = {0};
        strcat(out_path, out_dir);
        strcat(out_path, name);
        strcat(out_path, ".png");

        fprintf(cmd_fp,
                        "set terminal png transparent size 1280,960\n"
                        "set size 1.0,1.0\n"
                        "set output '%s'\n"
                        "set key left top\n"
                        "set yrange [0:]\n"
                        "set xdata time\n"
                        "set timefmt \"%%s\"\n"
                        "set format x \"%%Y-%%m-%%d\"\n"
                        "set grid y\n"
                        "set ylabel \"%s\"\n"
                        "set xtics rotate\n"
                        "set bmargin 6\n"
                        "plot ",
                        out_path, y_label);
        for (int i = 0; i < usercount; i++) {
                fprintf(cmd_fp,
                                "'%s' using 1:%d title \"%s\" w %s lw 4",
                                data_path, i+2, usernames[i], step_type);
                if (i != usercount - 1) {
                        fprintf(cmd_fp, ", ");
                } else {
                        fprintf(cmd_fp, "\n");
                }
        }

        /* Open a gnuplot process */
        FILE *gplot = popen(gnuplot_cmd, "w");
        if (gplot == NULL) {
                fprintf(stderr,
                                "Could not open process '%s'.\n",
                                gnuplot_cmd);
                exit(EXIT_FAILURE);
        }

        rewind(cmd_fp);
        char c = fgetc(cmd_fp);
        while (c != EOF) {
                fputc(c, gplot);
                c = fgetc(cmd_fp);
        }


        int exit_code = pclose(gplot);
        if (exit_code != EXIT_SUCCESS) {
                fprintf(stderr,
                                "gnuplot exited with bad return code: '%d'",
                                exit_code);
        }

        fclose(cmd_fp);
}

static int get_usernames (char *usernames[], const event_seq *seq) {
        int usercount = 0;
        // Loop through every user.
        for (
                        event_node *current = seq->first;
                        current != NULL;
                        current = current->next
            ) {
                char *user = current->user;
                // Test if the current user is unique.
                bool unique = true;
                for (int i = 0; i < usercount; i++) {
                        if (!strcmp(user, usernames[i])) {
                                unique = false;
                                break;
                        }
                }
                // If the user is unique, add them to the array of usernames.
                if (unique) {
                        size_t username_size = strlen(user) + 1;
                        usernames[++usercount-1] = malloc(username_size);
                        strcpy(usernames[usercount-1], user);
                }
        }
        return usercount;
}

static void inflate_file (const char *file_path, FILE *out_file) {
        /* Open the input file. */
        FILE *fp = fopen (file_path, "rb");
        if (fp == NULL) {
                fprintf(stderr,
                                "Could not open file %s.\n",
                                file_path);
                exit(EXIT_FAILURE);
        }

        z_stream strm = {0};
        unsigned char in[CHUNK];
        unsigned char out[CHUNK];

        strm.zalloc = Z_NULL;
        strm.zfree = Z_NULL;
        strm.opaque = Z_NULL;
        strm.next_in = in;
        strm.avail_in = 0;

        int zstatus;
        if ((zstatus = inflateInit2 (&strm, windowBits | ENABLE_ZLIB_GZIP)) != Z_OK) {
                fprintf(stderr,
                                "`inflateInit2` returned a bad status of %d.\n",
                                zstatus);
                exit(EXIT_FAILURE);
        }

        while (1) {
                int bytes_read;
                int zlib_status;

                bytes_read = fread (in, sizeof (char), sizeof (in), fp);
                if (ferror (fp)) {
                        fprintf(stderr,
                                        "Could not read file %s.\n",
                                        file_path);
                        exit(EXIT_FAILURE);
                }
                strm.avail_in = bytes_read;
                strm.next_in = in;
                do {
                        unsigned have;
                        strm.avail_out = CHUNK;
                        strm.next_out = out;
                        zlib_status = inflate (& strm, Z_NO_FLUSH);
                        switch (zlib_status) {
                                case Z_OK:
                                case Z_STREAM_END:
                                case Z_BUF_ERROR:
                                        break;

                                default:
                                        inflateEnd (& strm);
                                        fprintf (stderr, "Gzip error %d in %s.\n",
                                                        zlib_status, file_path);
                                        exit(EXIT_FAILURE);
                        }
                        have = CHUNK - strm.avail_out;
                        fwrite (out, sizeof (unsigned char), have, out_file);
                }
                while (strm.avail_out == 0);
                if (feof (fp)) {
                        inflateEnd (& strm);
                        break;
                }
        }
        if (fclose (fp)) {
                fprintf(stderr,
                                "Could not close file %s.\n",
                                file_path);
                exit(EXIT_FAILURE);
        }
}

static time_t get_date_from_file_path (const char *file_path) {
        // Isolate the file name from the path.
        char *file_name = basename(file_path);      // GNU version of `basename`.
        if (file_name == NULL) {
                fprintf(stderr,
                                "%s is not a valid file path.\n",
                                file_path);
                exit(EXIT_FAILURE);
        }
        // Put the time into a broken-down time struct.
        struct tm btime = {0};
        strptime(file_name, "%F", &btime);
        return mktime(&btime);
}
