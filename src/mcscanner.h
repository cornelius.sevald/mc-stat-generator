#ifndef MCSCANNER_H
#define MCSCANNER_H

#include <time.h>
#include "mcevents.h"

#define NEWLINE_TOKEN		    1
#define OPEN_SBRACKET_TOKEN	    2
#define CLOSED_SBRACKET_TOKEN	3
#define OPEN_ABRACKET_TOKEN	    4
#define CLOSED_ABRACKET_TOKEN	5
#define OPEN_PBRACKET_TOKEN	    6
#define CLOSED_PBRACKET_TOKEN	7
#define COLON_TOKEN			    8
#define FSLASH_TOKEN			9
#define INFO_TOKEN		    	10
#define TIMESTAMP_TOKEN	    	11
#define IP_TOKEN				12
#define NATURAL_TOKEN			13
#define INTEGER_TOKEN			14
#define FLOAT_TOKEN		    	15
#define USERNAME_TOKEN	    	16
#define LOGIN_TOKEN		    	17
#define LOGOUT_TOKEN			18
#define ACHIEVEMENT_TOKEN		19
#define MESSAGE_TOKEN			20
#define DEATH_TOKEN		    	21

extern const char *token_names[];

event_seq *scan_file (FILE *fp);
void scanner_destroy ();

#endif /* MCSCANNER_H */
