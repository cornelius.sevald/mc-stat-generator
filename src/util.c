#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include "util.h"

static int string_comp (const void *a, const void *b);

void sort_strings (char *string_array[], int string_count) {
        qsort (string_array, string_count, sizeof(const char*), string_comp);
}

static int string_comp (const void *a, const void *b) {
        return strcmp(*(const char**)a, *(const char**)b); 
}

FILE *make_tmp_file (char *template) {
        int fd = -1;
        FILE *fp;
        if ((fd = mkstemp(template)) == -1 ||
                        (fp = fdopen(fd, "w")) == NULL) {
                if (fd != -1) {
                        unlink(template);
                        close(fd);
                }
                fprintf(stderr, "%s: %s\n", template, strerror(errno));
                return (NULL);
        } 
        return fp;
}
