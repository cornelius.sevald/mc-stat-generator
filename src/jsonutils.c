#include <stdarg.h>
#include "jsonutils.h"

#define SPACES_PER_INDENT 4

void write_indent (FILE *fp, int indent_level) {
        for (int i = 0; i < indent_level * SPACES_PER_INDENT; i++) {
                fputc(' ', fp);
        }
}

void write_json (FILE *fp, int indent_level, const char *fmt, ...) {
        write_indent(fp, indent_level);

        va_list args;
        va_start(args, fmt);
        vfprintf(fp, fmt, args);
        va_end(args);
}

void write_int_stats (FILE *fp, int indent_level, int usercount,
                const char *label, char *usernames[], int stats[]) {
        write_json(fp, indent_level,    "\"%s\":\n", label);
        write_json(fp, indent_level++,  "{\n");
        for (int i = 0; i < usercount; i++) {
                char *user = usernames[i];
                int  stat  = stats[i];

                write_json(fp, indent_level, "\"%s\": %d", user, stat);
                if (i != usercount - 1) {
                        fprintf(fp, ",\n");
                } else {
                        fprintf(fp, "\n");
                }
        }
        write_json(fp, --indent_level, "}");
}


