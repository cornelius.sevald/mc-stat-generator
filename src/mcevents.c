#include <stdlib.h>
#include "mcevents.h"

void add_event_node (event_seq *seq, event_node *node) {
        if (seq->first == NULL) {
                seq->first = node;
                return;
        }
        event_node *current = seq->first;
        while (current->next != NULL) {
                current = current->next;
        }
        current->next = node;
}

void init_event_sequence (event_seq *seq) {
        seq->first = NULL;
}

void destroy_event_sequence (event_seq *seq) {
        event_node *current = seq->first;
        if (current == NULL) {
                return;
        }
        event_node *next = current->next;
        free(current);
        while (next != NULL) {
                current = next;
                next = next->next;
                free(current);
        }

        seq->first = NULL;
}

void append_event_sequence (event_seq *s1, event_seq *s2) {
        if (s1->first == NULL) {
                s1->first = s2->first;
                return;
        }
        event_node *current = s1->first;
        while (current->next != NULL) {
                current = current->next;
        }
        current->next = s2->first;
}

int event_sequence_length (event_seq *seq) {
        int counter = 0;
        for (
                        event_node *current = seq->first;
                        current != NULL;
                        current = current->next
            ) {
                counter++;
        }
        return counter;
}
