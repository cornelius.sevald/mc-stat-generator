lex = $(wildcard src/*.l)
yy  = src/lex.yy.c
src = $(wildcard src/*.c) $(yy)
obj = $(src:.c=.o)

CFLAGS = -g -Wall -Wextra
LDFLAGS = -lz

mcsg: $(obj)
	$(CC) -o $@ $^ $(LDFLAGS)

$(obj): $(src)
	$(CC) -c $(src) $(CFLAGS)
	mv *.o src/

$(yy): $(lex)
	flex $(lex)
	mv lex.yy.c src/

.PHONY: clean
clean:
	rm -f $(obj) $(yy) mcsg

